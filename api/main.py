from unicodedata import category
from fastapi import FastAPI
import psycopg
from pydantic import BaseModel

app = FastAPI()

@app.get("/api/categories")
async def list_categories(page: int = 0):
  connection_string = "postgresql://trivia-game:trivia-game@db/trivia-game"
  with psycopg.connect(connection_string) as conn:
    with conn.cursor() as cur:
      cur.execute("""
      SELECT id, title, canon
      FROM categories
      """)
      records = cur.fetchall()
      results = []
      for record in records:
        result = {
          "id": record[0],
          "title": record[1],
          "canon": record[2],
        }
        results.append(result)

  return results

@app.get("/api/category/{category_id}")
def get_category(category_id: int):
  connection_string = "postgresql://trivia-game:trivia-game@db/trivia-game"
  with psycopg.connect(connection_string) as conn:
    with conn.cursor() as cur:
      cur.execute("""
      SELECT id, title, canon
      FROM categories
      WHERE id = %s
      """, [category_id])
      record = cur.fetchone()
      return {
          "id": record[0],
          "title": record[1],
          "canon": record[2],
        }

class Category(BaseModel):
  title: str
  canon: bool | None = False

@app.post("/api/categories")
def create_category(category: Category):
  dictionary = category.dict()
  return category

@app.put("/api/category/{category_id}")
async def update_category(category_id: int, category: Category, q: str | None = None):
  result = {"id": category_id, **category.dict()}
  if q:
    result.update({"q": q})
  return result
